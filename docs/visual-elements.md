# 시각적 요소

> [2단계](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/understanding_gui/src/step_02.py) 또는 모든 단계에 대한 코드를 [여기서](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/understanding_gui/src/src.zip) 다운로드받을 수 있다.

## Step 2: 시각적 요소
Mardown 구문을 사용할 때 Taipy는 [**시각적 요소**](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/)의 개념으로 이를 보강하고 있다. 시각적 요소는 클라이언트에 표시되는 Taipy 그래픽 객체이다. 객체는 [슬라이더(slider)](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/slider/), [차트](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/chart/), [테이블](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/table/), [입력](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/input/), [메뉴](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/menu/) 등이다. 목록을 [여기](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/controls/)에서 볼 수 있다.

모든 시각적 요소는 유사한 구문을 따르고 있다.

```
<|{variable}|visual_element_name|param_1=param_1|param_2=param_2| ... |>.
```

예를 들어, 다음과 같은 방식으로 `slider`를 작성한다.

```
<|{variable}|slider|min=min_value|max=max_value|>.
```

원하는 각 시각적 요소를 웹 페이지에 포함시키려면 페이지를 나타내는 마크다운 문자열이 위에서 언급한 시각적 요소 구문을 포함해야 한다. 예를 들어, 페이지의 시작 부분에 디스플레이하려면 다음과 같아야 한다.

- Python 변수 `text`
- `text` 변수 값을 "visually" 변경하는 입력

전반적인 구문은 다음과 같다.

```
<|{text}|>
<|{text}|input|>
```

다음은 합쳐진 코드이다.

```python
from taipy.gui import Gui

text = "Original text"

page = """
# Getting started with Taipy GUI

My text: <|{text}|>

<|{text}|input|>
"""

Gui(page).run()
```

![](./images/screenShot_02-01.png)
