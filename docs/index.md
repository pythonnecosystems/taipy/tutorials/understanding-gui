# GUI의 이해

> **Supported Python Versions**
> 
> Taipy는 Python 3.8 이상에서 실행된다.

Taipy 프론트엔드를 사용하는 튜토리얼에 오신 것을 환영한다. 이 튜토리얼에서는 Taipy를 사용하여 대화형 웹 어플리케이션을 구축하는 방법을 설명한다.

![](./images/result_01_01.webp)

Taipy는 웹 어플리케이션 개발을 단순화하는 것을 목표로 한다.

- 어플리케이션 구축을 가속화한다.
- 변수와 이벤트 관리를 간소화한다.
- 마크다운 구문을 사용하여 직관적인 시각화를 제공한다.

튜토리얼의 각 부분에서 Taipy의 기본 원칙을 강조할 것이다. 중요한 것은 각 단계가 이전 단계의 코드를 기반으로 한다는 것이다. 마지막 단계가 끝날 때쯤에는 자신만의 Taipy 어플리케이션을 만들 수 있는 능력을 갖추게 될 것이다.

## 시작 전
Taipy 패키지는 Python 3.8 이상이 필요하다.

```bash
$ pip install taipy
```

5 단계를 마치면 어플리케이션 시연을 위한 NLP(Natural Language Processing) 알고리즘이 포함되어 있다. 이 알고리즘은 Python 버전 3.8~3.10에서만 호환된다는 점에 주의하세요. 이 NLP 기능을 통합하려면 Transformer와 Torch를 설치해야 한다. 그러나 원한다면 이 알고리즘을 사용하지 않고도 튜토리얼 가이드를 진행할 수 있다.

```bash
$ pip install torch
$ pip install transformer
```

> **Info**:<br>
> `pip install taipy`는 안정적인 최신 버전의 Taipy를 설치하기 위해 선호되는 방법이다.
>
> ['pip`](https://pip.pypa.io/)이 설치되어 있지 않은 경우 이 [Python Installation Guide](http://docs.python-guide.org/en/latest/starting/installation/)는 설치 절차를 안내하고 있다.

## Notebook 사용
이 **튜토리얼**은 Python 스크립트(.py) 전용이다. **Jupyter Notebook**을 사용하려면 이 [`notebook`](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/understanding_gui/tutorial.ipynb)을 다운로드하세오.

## Taipy Studio
[Taipy Studio](https://docs.taipy.io/en/release-3.0/manuals/studio/)는 Taipy 시각적 요소들의 자동 완성을 제공하는 VS Code extension으로, Taipy Studio를 통해 Taipy 어플리케이션을 보다 쉽고 빠르게 만들 수 있다.

더 이상 지체하지 말고 코딩을 시작해 보세요!

## 단계
1. [첫 웹 페이지](./first-web-page.md)
1. 시각적 요소(Visual elements)
1. 상호작용
1. 차트
1. 속성의 Python 표현식
1. 페이지 레이아웃
1. 다중 페이지(Multi-pages), navibars와 메뉴

