# 속성의 Python 표현식
> [5단계](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/understanding_gui/src/step_05.py) 또는 모든 단계에 대한 코드를 [여기서](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/understanding_gui/src/src.zip) 다운로드받을 수 있다.

## Step 5: 속성의 Python 표현식
앞에서 보았듯이 Taipy의 매개변수와 변수는 동적이다. 모든 객체 타입, 심지어 데이터 프레임에도 동일하게 적용된다. 따라서 데이터 프레임에 대한 연산을 수행할 수 있으며, Taipy GUI는 GUI상에 실시간 결과를 보여준다. 이러한 변경 사항은 `state.xxx = yy(state.text = "Example")`와 같은 `=` 할당을 통해 발생된다.

마크다운에 `xxx`를 포함하는 식은 변경 사항을 전파하고 관련 요소를 다시 로드한다. 단순한 차트나 표일 수도 있지만 다음과 같은 식일 수도 있다.

```python
"""
## Positive
<|{np.mean(dataframe["Score Pos"])}|text|>

## Neutral
<|{np.mean(dataframe["Score Neu"])}|text|>

## Negative
<|{np.mean(dataframe["Score Neg"])}|text|>
"""
```

이러한 종류의 표현은 코딩 없이 시각적 요소 사이에 직접적인 연결을 만든다.

## NLP에서의 사용 사례 - Part 1
Taipy와 직접적인 관련은 없지만 여기에 NLP 코드를 제공한다. 이 NLP 엔진에 GUI를 랩하면 2부에서 작동하게 된다.

이 단계를 수행하기 전에 `pip install torch`와 `pip install transformers`를 수행하여야 한다. 이 코드에는 모델을 다운로드하여 사용된다. Torch는 현재 3.8에서 3.10(?) 사이의 Python 버전에서만 실행할 수 있다.

이러한 패키지를 설치하는 데 어려움이 있는 경우 `analy_text(text)` 함수의 출력으로 난수 사전을 제공하기만 하면 된다.

```python
from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification
from scipy.special import softmax


MODEL = f"cardiffnlp/twitter-roberta-base-sentiment"
tokenizer = AutoTokenizer.from_pretrained(MODEL)
model = AutoModelForSequenceClassification.from_pretrained(MODEL)


def analyze_text(text):
    # Run for Roberta Model
    encoded_text = tokenizer(text, return_tensors="pt")
    output = model(**encoded_text)
    scores = output[0][0].detach().numpy()
    scores = softmax(scores)

    return {"Text": text,
            "Score Pos": scores[2],
            "Score Neu": scores[1],
            "Score Neg": scores[0],
            "Overall": scores[2]-scores[0]}
```

## NLP에서의 사용 사례 - Part 2
아래 코드는 이 개념을 적용하여 생성된 데이터 프레임에 메트릭을 만든다.

```python
import numpy as np
import pandas as pd
from taipy.gui import Gui, notify

text = "Original text"

dataframe = pd.DataFrame({"Text": [""],
                          "Score Pos": [0.33],
                          "Score Neu": [0.33],
                          "Score Neg": [0.33],
                          "Overall": [0]})


def local_callback(state):
    notify(state, "Info", f"The text is: {state.text}", True)
    temp = state.dataframe.copy()
    scores = analyze_text(state.text)
    temp.loc[len(temp)] = scores
    state.dataframe = temp
    state.text = ""


page = """
<|toggle|theme|>

# Getting started with Taipy GUI

My text: <|{text}|>

Enter a word:
<|{text}|input|>
<|Analyze|button|on_action=local_callback|>

## Positive
<|{np.mean(dataframe["Score Pos"])}|text|format=%.2f|>

## Neutral
<|{np.mean(dataframe["Score Neu"])}|text|format=%.2f|>

## Negative
<|{np.mean(dataframe["Score Neg"])}|text|format=%.2f|>

<|{dataframe}|table|>

<|{dataframe}|chart|type=bar|x=Text|y[1]=Score Pos|y[2]=Score Neu|y[3]=Score Neg|y[4]=Overall|color[1]=green|color[2]=grey|color[3]=red|type[4]=line|>
"""

Gui(page).run()
```

![](./images/screenShot_05.png)
