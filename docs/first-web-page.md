# 첫 웹 페이지
> [1단계](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/understanding_gui/src/step_01.py) 또는 모든 단계에 대한 코드를 [여기서](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/understanding_gui/src/src.zip) 다운로드받을 수 있다.

## Step 1: 첫 웹 페이지
첫 Taipy 웹 페이지를 만들려면 코드 한 줄만 있으면 된다. 문자열로 GUI 객체를 만들어 실행하기만 하면 된다.

콘솔에서 클라이언트 링크를 찾을 수 있다. 첫 Taipy 페이지를 열기 위해 링크를 복사하고 웹 브라우저에 붙여넣기만 하면 된다!

```python
from taipy import Gui

Gui(page="# Getting started with *Taipy*").run() # use_reloader=True
```

기본적으로 코드를 수정한 후 페이지가 자동으로 새로 고쳐지지 않는다.

이 동작을 수행하도록 변경하려면 `run()` 메서드의 `use_reloader` 매개 변수를 `True`로 설정해야 한다. 그러면 어플리케이션에서 파일을 변경하고 저장할 때 어플리케이션을 자동으로 다시 로드한다. 일반적으로 개발 모드에서 사용된다.

여러 서버를 동시에 실행하고자 하는 경우, `run()` 메서드에서 서버 포트 번호(기본값으로 5000)를 수정할 수 있다. 예를 들어, `Gui(...).run(port=xxxxxx)`. `run()` 메서드에 대한 다른 매개 변수에 대한 정보를 [여기](https://docs.taipy.io/en/release-3.0/manuals/gui/configuration/#configuring-the-gui-instance)에서 얻을 수 있다.

텍스트 형식을 지정할 수 있는 옵션이 있다는 것을 명심하세요. Taipy는 페이지를 만들기 위해 [Markdown](https://docs.taipy.io/en/release-3.0/manuals/gui/pages/#using-markdown), [HTML](https://docs.taipy.io/en/release-3.0/manuals/gui/pages/#using-html) 또는 [Python 코드](https://docs.taipy.io/en/release-3.0/manuals/gui/page_builder/) 등 다양한 방법을 사용할 수 있다. 여기 텍스트 등을 스타일화하기 위한 마크다운 구문이 있다. 따라서 `#`는 제목을 만들고 `##`은 부제를 만듭니다. 텍스트에 *이탤릭체*의 경우 `*`를 넣거나 **굵게** 표시하려면 `**`를 넣는다.

![](./images/screenshot_1_01.png)
