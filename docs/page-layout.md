# 페이지 레이아웃
> [6단계](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/understanding_gui/src/step_06.py) 또는 모든 단계에 대한 코드를 [여기서](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/understanding_gui/src/src.zip) 다운로드받을 수 있다.

## Step 6: 페이지 레이아웃
몇 단계 만에 다양한 매개 변수로 며칠 동안 예측할 수 있는 포괄적인 예측 어플리케이션을 성공적으로 구축했다. 그럼에도 불구하고 페이지 레이아웃은 크게 개선될 여지가 있다. 페이지의 시각적 매력을 강화하기 위해 유용한 새로운 컨트롤 3가지를 소개한다. 이 컨트롤은 다음과 같다.

- [**part**](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/part/): 텍스트/시각 요소 그룹을 만든다. `part`의 유용한 속성은 *렌더링*이다. False로 설정하면 part가 디스플레이되지 않는다. 이를 통해 개발자가 시각 요소 그룹을 동적으로 숨길 수 있다.

```python
<|part|render={bool_variable}|Text Or visual elements...|>
```

- [**레이아웃**](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/layout/): 텍스트와 시각적 요소를 넣을 수 있는 보이지 않는 열을 만든다. 열 속성은 열의 너비와 수를 나타낸다. 여기서 우리는 같은 너비의 세 개의 열을 만든다.

```python
<|layout|columns=1 1 1|
Button in first column <|Press|button|>

Second column

Third column
|>
```

![Fig](./images/layout.png)

[**expandable**](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/expandable/): 확장하거나 축소할 수 있는 블록을 만든다.

![Fig](./images/expandable.png)

## 전체 코드

```python
from taipy.gui import Gui, notify
import pandas as pd
import numpy as np


text = "Original text"

dataframe = pd.DataFrame({"Text":['Test', 'Other', 'Love'],
                          "Score Pos":[1, 1, 4],
                          "Score Neu":[2, 3, 1],
                          "Score Neg":[1, 2, 0],
                          "Overall":[0, -1, 4]})

property_chart = {"type": "bar",
                  "x": "Text",
                  "y[1]": "Score Pos",
                  "y[2]": "Score Neu",
                  "y[3]": "Score Neg",
                  "y[4]": "Overall",
                  "color[1]": "green",
                  "color[2]": "grey",
                  "color[3]": "red",
                  "type[4]": "line"
                 }

def on_button_action(state):
    notify(state, 'info', f'The text is: {state.text}')
    state.text = "Button Pressed"

# def on_change(state, var_name, var_value):
#     if var_name == "text" and var_value == "Reset":
#         state.text = ""
#         return

page = """
<|toggle|theme|>

# Getting started with Taipy GUI

<|layout|columns=1 1|
<|
My text: <|{text}|>

Enter a word:
<|{text}|input|>
<|Analyze|button|on_action=on_button_action|>
|>


<|Table|expandable|
<|{dataframe}|table|width=100%|>
|>
|>

<|layout|columns=1 1 1|
## Positive <|{np.mean(dataframe['Score Pos'])}|text|format=%.2f|raw|>

## Neutral <|{np.mean(dataframe['Score Neu'])}|text|format=%.2f|raw|>

## Negative <|{np.mean(dataframe['Score Neg'])}|text|format=%.2f|raw|>
|>

<|{dataframe}|chart|type=bar|x=Text|y[1]=Score Pos|y[2]=Score Neu|y[3]=Score Neg|y[4]=Overall|color[1]=green|color[2]=grey|color[3]=red|type[4]=line|>
"""

Gui(page).run(use_reloader=True)

```

![](./images/screenShot_06.png)
