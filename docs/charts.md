# 차트

> [4단계](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/understanding_gui/src/step_04.py) 또는 모든 단계에 대한 코드를 [여기서](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/understanding_gui/src/src.zip) 다운로드받을 수 있다.

## Step 4: 차트
차트는 Taipy (그리고 모든 웹 어플리케이션)의 필수적인 부분이다. 차트는 그것을 커스텀하기 위해 많은 속성을 가진 또 다른 시각적 요소일 뿐이다.

차트를 만드는 가장 간단한 코드 중 하나는 다음과 같다.

```python
list_to_display = [100/x for x in range(1, 100)]
Gui("<|{list_to_display}|chart|>").run()
```

리스트, Numpy 배열 또는 Pandas Dataframe과 같은 다양한 형식을 차트 요소에 전달할 수 있다.

## 다양한 유용한 속성
Taipy 차트는 플로틀리(Plotly) 차트를 기반으로 한다. 다른 어떤 시각적 요소보다 차트는 많은 속성을 가지고 있다.

여기에서는 몇 가지 중요한 속성만 다를 것이다. 자세한 내용은 [문서](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/chart/)를 찾아 보세오. 

- 차트의 축을 정의하는 데 `x`와 `y`를 사용한다. 열 내부의 데이터가 동적이어도 차트에 표시할 열의 이름은 그렇지 않다.

```python
data = {"x_col": [0, 1, 2], "y_col1": [4, 1, 2]}
Gui("<|{data}|chart|x=x_col|y=y_col1|>").run()
```

- `x`와 `y`를 색인화하여 차트에 더 많은 트레이스를 추가할 수 있다.

```python
data = {"x_col": [0, 1, 2], "y_col_1": [4, 2, 1], "y_col_2": [3, 1, 2]}
Gui("<|{data}|chart|x=x_col|y[1]=y_col_1|y[2]=y_col_2|>").run()
```

-  Taipy는 그래프를 커스터마이즈할 수 있는 다양한 옵션을 제공한다. 색상은 그 중 하나이다.

```python
data = {"x_col": [0, 1, 2], "y_col_1": [4, 2, 1], "y_col_2":[3, 1, 2]}
Gui("<|{data}|chart|x=x_col|y[1]=y_col_1|y[2]=y_col_2|color[1]=green|>").run()
```

## 다양한 차트 종류
지도, 막대 차트, 파이 차트, 선 차트, 3D 차트 등 다양한 타입을 사용할 수 있다. 빠르게 사용하는 방법을 알기 위해 [여기](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/chart/)에 타입이 나열되어 있다. 호환되는 경우 `scatter`, `line_`와 `bar`같은 두 가지 유형을 동일한 차트에서 함께 사용할 수도 있다.

```python
data = {"x_col": [0, 1, 2], "y_col_1": [4, 1, 2], "y_col_2": [3, 1, 2]}
Gui("<|{data}|chart|x=x_col|y[1]=y_col_1|y[2]=y_col_2|type[1]=bar|>").run()
```

## 코드
코드에 차트를 추가하여 NLP 알고리즘이 다양한 문장에 부여한 점수를 시각화한다.

```python
page = """
... put the previous Markdown page here

<|{dataframe}|table|>

<|{dataframe}|chart|type=bar|x=Text|y[1]=Score Pos|y[2]=Score Neu|y[3]=Score Neg|y[4]=Overall|color[1]=green|color[2]=grey|color[3]=red|type[4]=line|>
"""


dataframe = pd.DataFrame({"Text":['Test', 'Other', 'Love'],
                          "Score Pos":[1, 1, 4],
                          "Score Neu":[2, 3, 1],
                          "Score Neg":[1, 2, 0],
                          "Overall":[0, -1, 4]})
```

## 시각적 요소 작성을 위한 빠른 팁
코딩을 쉽게 하기 위해 각 시각 요소을 속성의 Python 사전으로 직접 설정할 수 있는 `properties`이라는 속성이 있다. 위에 표시된 그래프를 다시 만들려면 다음을 수행해야 한다.

```python
property_chart = {"type": "bar",
                  "x": "Text",
                  "y[1]": "Score Pos",
                  "y[2]": "Score Neu",
                  "y[3]": "Score Neg",
                  "y[4]": "Overall",
                  "color[1]": "green",
                  "color[2]": "grey",
                  "color[3]": "red",
                  "type[4]": "line"
                 }

page = """
...
<|{dataframe}|chart|properties={property_chart}|>
...
"""
```

![](./images/screenShot_04.png)
