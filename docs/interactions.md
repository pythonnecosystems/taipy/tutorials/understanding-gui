# 상호작용

> [3단계](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/understanding_gui/src/step_03.py) 또는 모든 단계에 대한 코드를 [여기서](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/understanding_gui/src/src.zip) 다운로드받을 수 있다.

## Step 3: 상호작용
이제 페이지에는 다음과 같은 몇 가지 시각적 요소가 있다:.

- Python 변수 `text`에 연결된 text
- 값 `text`를 자동으로 변경하는 input

Taipy GUI는 백그라운드의 모든 것을 관리한다.

Taipy GUI로 더 나아가기 위해 **상태(state)** 개념을 소개하려고 한다. 이러한 상태 개념 덕분에 Taipy는 기본적으로 다중 사용자 GUI 앱을 제공하고 있다.

## 다중 사용자 - 상태
같은 URL을 가진 클라이언트를 몇 개 열어 보세요. 모든 클라이언트가 서로 독립적이라는 것을 알 수 있을 것이고, 클라이언트에서 텍스트를 변경할 수 있으며, 다른 클라이언트에서는 텍스트가 변경되지 않을 것이다. 이것은 상태의 개념 때문이다.

상태는 하나의 특정 연결에 대해 사용자 인터페이스에 사용되는 모든 변수의 값을 유지한다.

예를 들어, 처음에 `state.text = 'Original text'`가 있다. 입력에 의해 (주어진 그래픽 클라이언트를 통해) 텍스트가 수정되면, 이는 사실상 `text`(글로벌 파이썬 변수)가 아닌 `state.text`가 수정된다. 따라서 두 개의 다른 클라이언트를 열면 `text`는 각 클라이언트에 대해 하나씩 두 개의 상태 값(`state.text`)을 갖게 된다.

아래 코드에서 이 개념은 다음을 위해 사용된다.

- 버튼을 누르면 사용자에게 알림;
- 텍스트가 "Reset(재설정)"이면 입력을 재설정한다.

## 두 변수를 연결하는 방법 - [`on_change`](https://docs.taipy.io/en/release-3.0/manuals/gui/callbacks/) 콜백
Taipy에서 `on_change()` 함수는 "특수" 함수이다. **Taipy**는 당신이 만들었는지 확인하고 이 이름을 가진 함수를 사용한다. 변수의 상태가 수정될 때마다 세 매개 변수로 콜백 함수를 호출한다.

- state(모든 변수를 포함하는 상태 객체)
- 수정된 변수의 이름
- 이 변수의 새 값

여기서 텍스트의 값(`state.text`)이 변경될 때마다 `on_change()`가 호출된다. 이 함수에서 변수가 변경되면 Taipy는 이 변경 사항을 관련 시각적 요소에 자동으로 전파한다.

시각적 요소에 특화된 다른 콜백들이 존재한다. 그들은 `on_change` 또는 `on_action`이라고 부른다. 예를 들어, 버튼은 `on_action` 속성을 갖는다. 버튼을 누르면 Taipy는 `on_action` 속성이 참조하는 콜백 함수를 호출한다.

```python
from taipy.gui import Gui, notify

text = "Original text"

# Definition of the page
page = """
# Getting started with Taipy GUI

My text: <|{text}|>

<|{text}|input|>

<|Run local|button|on_action=on_button_action|>
"""

def on_button_action(state):
    notify(state, 'info', f'The text is: {state.text}')
    state.text = "Button Pressed"

def on_change(state, var_name, var_value):
    if var_name == "text" and var_value == "Reset":
        state.text = ""
        return


Gui(page).run()
```

![](./images/screenShot_03_01.png)

[`notify()`](https://docs.taipy.io/en/release-3.0/manuals/reference/taipy.gui.notify/)는 일부 정보에 대한 [알림](https://docs.taipy.io/en/release-3.0/manuals/gui/notifications/)을 생성하는 Taipy GUI 함수이다. 사용자는 `state`, `notification_type`과 `message`를 포함한 여러 매개 변수를 전달할 수 있다.
